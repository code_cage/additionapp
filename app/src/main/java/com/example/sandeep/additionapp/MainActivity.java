package com.example.sandeep.additionapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText txtOne;
    EditText txtTwo;
    Button btnAdd;
    TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add();
            }
        });
    }

    private void add() {

        int a= Integer.parseInt(txtOne.getText().toString());
        int b= Integer.parseInt(txtTwo.getText().toString());
        int c=a+b;

        tvResult.setText(String.valueOf(c));
    }

    private void init() {

        txtOne=findViewById(R.id.txt_one);
        txtTwo=findViewById(R.id.txt_two);
        btnAdd=findViewById(R.id.btn_add);
        tvResult=findViewById(R.id.tv_result);
    }
}
